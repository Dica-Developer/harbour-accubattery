import QtQuick 2.0
import Sailfish.Silica 1.0


Page {
    property string name: "AccuBattery"
    property string year: (new Date()).getFullYear()
    property string version: app.version
    property string imagelocation: "/usr/share/icons/hicolor/86x86/apps/harbour-accubattery.png"
    property string url: "https://gitlab.com/Dica-Developer/harbour-accubattery"
    property string urlText: name + " " + qsTr("on gitlab.com")

    id: about

    SilicaFlickable
    {
        anchors.fill: parent

        contentHeight: column.height

        Column
        {
            id: column

            width: about.width
            spacing: Theme.paddingLarge
            PageHeader
            {
                title: "About " + name
            }
            Label
            {
                x: Theme.paddingLarge
                text: name
                color: Theme.highlightColor
                font.pixelSize: Theme.fontSizeMedium
                font.bold: true
                anchors.horizontalCenter: parent.horizontalCenter
            }

            Rectangle
            {
                visible: imagelocation.length > 0
                anchors.horizontalCenter: parent.horizontalCenter
                height: Theme.iconSizeLauncher
                width: Theme.iconSizeLauncher
                color: "transparent"

                Image
                {
                    visible: imagelocation.length > 0
                    source: imagelocation
                    anchors.fill: parent
                }
            }

            Button {
                x: Theme.paddingLarge
                text:  urlText
                onClicked: Qt.openUrlExternally(url)
                color: Theme.primaryColor
                anchors.horizontalCenter: parent.horizontalCenter
            }
            Label {
                x: Theme.paddingLarge
                text: "(C) " + year + " mschaaf"
                color: Theme.highlightColor
                font.pixelSize: Theme.fontSizeMedium
                anchors.horizontalCenter: parent.horizontalCenter
            }
            Label {
                x: Theme.paddingLarge
                text: "Version: " + version
                color: Theme.highlightColor
                font.pixelSize: Theme.fontSizeMedium
                anchors.horizontalCenter: parent.horizontalCenter
            }
        }
    }
}
