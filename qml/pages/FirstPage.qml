import QtQuick 2.0
import Sailfish.Silica 1.0

Page {
    id: page

    // The effective value will be restricted by ApplicationWindow.allowedOrientations
    allowedOrientations: Orientation.All

    // To enable PullDownMenu, place our content in a SilicaFlickable
    SilicaFlickable {
        anchors.fill: parent

        PullDownMenu {
            MenuItem {
                text: "About"
                onClicked: pageStack.push(Qt.resolvedUrl("About.qml"));
            }
            MenuItem {
                text: "Battery info"
                onClicked: pageStack.push(Qt.resolvedUrl("BatteryInfo.qml"));
            }
        }

        // Tell SilicaFlickable the height of its content.
        contentHeight: column.height

        // Place our content in a Column.  The PageHeader is always placed at the top
        // of the page, followed by our content.
        Column {
            id: column

            width: page.width
            spacing: Theme.paddingLarge
            PageHeader {
                title: app.isCharging ? qsTr("Charge") : qsTr("Discharge")
            }

            SectionHeader {
                text: qsTr("Now")
            }
            Label {
                text: app.current + " (" + pageTimeLeft + ")"
                width: page.width
                horizontalAlignment: Text.AlignLeft
                color: Theme.highlightColor
                font.pixelSize: Theme.fontSizeExtraLarge
            }

            SectionHeader {
                text: qsTr("Average 1m")
            }
            Text {
                text: app.pageAverageTimeLeft1m
                width: parent.width
                horizontalAlignment: Text.AlignLeft
                color: Theme.highlightColor
                font.pixelSize: Theme.fontSizeExtraLarge
            }
            SectionHeader {
                text: qsTr("Average 5m")
            }
            Text {
                text: app.pageAverageTimeLeft5m
                width: parent.width
                horizontalAlignment: Text.AlignLeft
                color: Theme.highlightColor
                font.pixelSize: Theme.fontSizeExtraLarge
            }
            SectionHeader {
                text: qsTr("Average 15m")
            }
            Text {
                text: app.pageAverageTimeLeft15m
                width: parent.width
                horizontalAlignment: Text.AlignLeft
                color: Theme.highlightColor
                font.pixelSize: Theme.fontSizeExtraLarge
            }
            SectionHeader {
                text: qsTr("Average Overall")
            }
            Text {
                text: app.pageAverageTimeLeft
                width: parent.width
                horizontalAlignment: Text.AlignLeft
                color: Theme.highlightColor
                font.pixelSize: Theme.fontSizeExtraLarge
            }

            TextSwitch {
                text: qsTr("Collect data once a second")
                onCheckedChanged: {
                    if (checked) {
                        timer.interval = 1000;
                    } else {
                        timer.interval = 60000;
                    }
                }
                checked: timer.interval < 60000
                width: parent.width
                x: Theme.horizontalPageMargin
            }
        }
    }
}
