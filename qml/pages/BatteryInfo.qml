import QtQuick 2.0
import Sailfish.Silica 1.0

Page {
    id: page

    // The effective value will be restricted by ApplicationWindow.allowedOrientations
    allowedOrientations: Orientation.All

    // To enable PullDownMenu, place our content in a SilicaFlickable
    SilicaFlickable {
        anchors.fill: parent

        PullDownMenu {
            MenuItem {
                text: "About"
                onClicked: pageStack.push(Qt.resolvedUrl("About.qml"));
            }
            MenuItem {
                text: "Dis-/Charge info"
                onClicked: pageStack.push(Qt.resolvedUrl("FirstPage.qml"));
            }
        }

        // Tell SilicaFlickable the height of its content.
        contentHeight: column.height

        // Place our content in a Column.  The PageHeader is always placed at the top
        // of the page, followed by our content.
        Column {
            id: column

            width: page.width
            spacing: Theme.paddingLarge
            PageHeader {
                title: qsTr("Battery")
            }

            SectionHeader {
                text: qsTr("Current")
            }
            Text {
                text: app.current
                width: page.width
                horizontalAlignment: Text.AlignLeft
                color: Theme.highlightColor
                font.pixelSize: Theme.fontSizeExtraLarge
            }

            SectionHeader {
                text: qsTr("Health")
            }
            Text {
                text: app.health
                width: page.width
                horizontalAlignment: Text.AlignLeft
                color: Theme.highlightColor
                font.pixelSize: Theme.fontSizeExtraLarge
            }

            SectionHeader {
                text: qsTr("Capacity")
            }
            Text {
                text: app.capacity
                width: page.width
                horizontalAlignment: Text.AlignLeft
                color: Theme.highlightColor
                font.pixelSize: Theme.fontSizeExtraLarge
            }

            SectionHeader {
                text: qsTr("Charge type")
            }
            Text {
                text: app.chargeType
                width: page.width
                horizontalAlignment: Text.AlignLeft
                color: Theme.highlightColor
                font.pixelSize: Theme.fontSizeExtraLarge
            }

            SectionHeader {
                text: qsTr("Charge full / design")
            }
            Text {
                text: app.chargeFull + "/" + app.chargeFullDesign
                width: page.width
                horizontalAlignment: Text.AlignLeft
                color: Theme.highlightColor
                font.pixelSize: Theme.fontSizeExtraLarge
            }

            SectionHeader {
                text: qsTr("Voltage now")
            }
            Text {
                text: app.voltageNow
                width: page.width
                horizontalAlignment: Text.AlignLeft
                color: Theme.highlightColor
                font.pixelSize: Theme.fontSizeExtraLarge
            }
        }
    }
}
