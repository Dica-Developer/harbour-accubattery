import QtQuick 2.0
import QtQuick.LocalStorage 2.0
import Sailfish.Silica 1.0
import "pages"

import harbour.sailfish.browser.search.engine.manager.dicadevelopers.utils.Files 1.0
import "js/accubattery.js" as AccuBattery

ApplicationWindow
{
    id: app

    // This declaration is necessary for usage of files in accubattery.js
    Files {
        id: files
    }

    property string version: "20.06"

    property var chargeDb: initDb();

    property bool isCharging: AccuBattery.isCharging()
    property string current: AccuBattery.getPowerConsumption()
    property string voltageNow: AccuBattery.getVoltageNow()
    property string inputCurrentNow: AccuBattery.getInputCurrentNow()
    property string chargeFull: AccuBattery.getChargeFull()
    property string chargeFullDesign: AccuBattery.getChargeFullDesign()
    property string chargeType: AccuBattery.getChargeType()
    property string chargeTypeOld: AccuBattery.getChargeType()
    property string capacity: AccuBattery.getCapacity()
    property string health: AccuBattery.getHealth()
    property string coverAverage: ""
    property string coverNow: (AccuBattery.getPowerConsumptionNumber() / 1000).toFixed(0) + "mA (" + getTimeLeft(AccuBattery.getPowerConsumptionNumber(), AccuBattery.getCurrentCharge()) + ")"
    property string pageTimeLeft: getTimeLeft(AccuBattery.getPowerConsumptionNumber(), AccuBattery.getCurrentCharge())
    property string pageAverageTimeLeft: "n.a."
    property string pageAverageTimeLeft1m: "n.a."
    property string pageAverageTimeLeft5m: "n.a."
    property string pageAverageTimeLeft15m: "n.a."

    Timer {
        id: timer
        interval: 60000
        running: true
        repeat: true
        onTriggered: app.update()
        onRunningChanged: app.reset()
    }

    function initDb() {
      var db = LocalStorage.openDatabaseSync("dicadeveloperAccuBattery", "", "AccuBattery Database", 1000000);
      if ("" === db.version) {
        db.changeVersion("", "1", function(tx) {
          tx.executeSql('CREATE TABLE IF NOT EXISTS Charge(isCharging BOOLEAN, current INTEGER, date DATETIME, chargeFull DOUBLE, capacity INTEGER)');
        });
      } else if ("1" === db.version) {
        // current version
      } else {
          console.log('ERROR: Unknow database version', db.version);
      }

      return db;
    }

    function update() {
        current = AccuBattery.getPowerConsumption();
        voltageNow = AccuBattery.getVoltageNow();
        inputCurrentNow = AccuBattery.getInputCurrentNow();
        chargeFull = AccuBattery.getChargeFull();
        chargeFullDesign = AccuBattery.getChargeFullDesign();
        chargeType = AccuBattery.getChargeType();
        capacity = AccuBattery.getCapacity();
        health = AccuBattery.getHealth();

        var currentNumber = AccuBattery.getPowerConsumptionNumber();
        var currentCharge = AccuBattery.getCurrentCharge();
        var chargeFullNumber = AccuBattery.getChargeFullNumber();

        chargeDb.transaction(
          function(tx) {
            tx.executeSql('INSERT INTO Charge VALUES(?, ?, ?, ?, ?)',  [AccuBattery.isCharging(), currentNumber, Date.now(), chargeFull, AccuBattery.getCapacityNumber()]);
          }
        )

        if (!AccuBattery.isCharging()) {
            chargeDb.readTransaction(
              function(tx) {
                var rs = tx.executeSql('SELECT capacity, date FROM Charge WHERE isCharging = 0 AND capacity IS NOT NULL AND date > ? ORDER BY date ASC LIMIT 1', [Date.now() - 3600000] );
                if (rs.rows.length > 0) {
                  var startDate = rs.rows.item(0).date;
                  var startCapacity = rs.rows.item(0).capacity;
                  rs = tx.executeSql('SELECT capacity, date FROM Charge WHERE isCharging = 0 AND capacity IS NOT NULL AND date > ? ORDER BY date DESC LIMIT 1', [Date.now() - 3600000] );
                  var endDate = rs.rows.item(0).date;
                  var endCapacity = rs.rows.item(0).capacity;
                  var dischargePerHour = ((startCapacity - endCapacity) * 3600) / ((endDate - startDate) / 1000);
                  coverAverage = '-' + dischargePerHour.toFixed(2) + '%/h';
                }
              }
            )
            chargeDb.readTransaction(
              function(tx) {
                var rs = tx.executeSql('select avg(current) AS currentAverage FROM Charge WHERE isCharging = 0');
                var currentAverage = rs.rows.item(0).currentAverage;
                pageAverageTimeLeft = "-" + (currentAverage / 1000).toFixed(0) + "mA (" + getTimeLeft(currentAverage, currentCharge) + ")";
              }
            )
            chargeDb.readTransaction(
              function(tx) {
                var rs = tx.executeSql('select AVG(current) AS currentAverage FROM Charge WHERE isCharging = 0 AND date > ?', [Date.now() - 300000]);
                var currentAverage = rs.rows.item(0).currentAverage;
                pageAverageTimeLeft5m = "-" + (currentAverage / 1000).toFixed(0) + "mA (" + getTimeLeft(currentAverage, currentCharge) + ")";
                coverAverage = "-" + (currentAverage / 1000).toFixed(0) + "mA " + coverAverage
              }
            )
            chargeDb.readTransaction(
              function(tx) {
                var rs = tx.executeSql('select AVG(current) AS currentAverage FROM Charge WHERE isCharging = 0 AND date > ?', [Date.now() - 60000]);
                var currentAverage = rs.rows.item(0).currentAverage;
                pageAverageTimeLeft1m = "-" + (currentAverage / 1000).toFixed(0) + "mA (" + getTimeLeft(currentAverage, currentCharge) + ")";
              }
            )
            chargeDb.readTransaction(
              function(tx) {
                var rs = tx.executeSql('select AVG(current) AS currentAverage FROM Charge WHERE isCharging = 0 AND date > ?', [Date.now() - 900000]);
                var currentAverage = rs.rows.item(0).currentAverage;
                pageAverageTimeLeft15m = "-" + (currentAverage / 1000).toFixed(0) + "mA (" + getTimeLeft(currentAverage, currentCharge) + ")";
              }
            )
        } else {
            chargeDb.readTransaction(
              function(tx) {
                var rs = tx.executeSql('SELECT capacity, date FROM Charge WHERE isCharging = 1 AND capacity IS NOT NULL AND date > ? ORDER BY date ASC LIMIT 1', [Date.now() - 3600000] );
                if (rs.rows.length > 0) {
                  var startDate = rs.rows.item(0).date;
                  var startCapacity = rs.rows.item(0).capacity;
                  rs = tx.executeSql('SELECT capacity, date FROM Charge WHERE isCharging = 1 AND capacity IS NOT NULL AND date > ? ORDER BY date DESC LIMIT 1', [Date.now() - 3600000] );
                  var endDate = rs.rows.item(0).date;
                  var endCapacity = rs.rows.item(0).capacity;
                  var chargePerHour = ((endCapacity - startCapacity) * 3.6) / (endDate - startDate);
                  coverAverage = chargePerHour.toFixed(2) + '%/h';
                }
              }
            )
            chargeDb.readTransaction(
              function(tx) {
                var rs = tx.executeSql('select avg(current) AS currentAverage FROM Charge WHERE isCharging = 1');
                var currentAverage = rs.rows.item(0).currentAverage;
                pageAverageTimeLeft = (currentAverage / 1000).toFixed(0) + "mA (" + getTimeLeft(currentAverage, chargeFullNumber - currentCharge) + ")";
              }
            )
            chargeDb.readTransaction(
              function(tx) {
                var rs = tx.executeSql('select AVG(current) AS currentAverage FROM Charge WHERE isCharging = 1 AND date > ?', [Date.now() - 300000]);
                var currentAverage = rs.rows.item(0).currentAverage;
                pageAverageTimeLeft5m = (currentAverage / 1000).toFixed(0) + "mA (" + getTimeLeft(currentAverage, currentCharge) + ")";
                coverAverage = "-" + (currentAverage / 1000).toFixed(0) + "mA " + coverAverage
              }
            )
            chargeDb.readTransaction(
              function(tx) {
                var rs = tx.executeSql('select AVG(current) AS currentAverage FROM Charge WHERE isCharging = 1 AND date > ?', [Date.now() - 60000]);
                var currentAverage = rs.rows.item(0).currentAverage;
                pageAverageTimeLeft1m = (currentAverage / 1000).toFixed(0) + "mA (" + getTimeLeft(currentAverage, currentCharge) + ")";
              }
            )
            chargeDb.readTransaction(
              function(tx) {
                var rs = tx.executeSql('select AVG(current) AS currentAverage FROM Charge WHERE isCharging = 1 AND date > ?', [Date.now() - 900000]);
                var currentAverage = rs.rows.item(0).currentAverage;
                pageAverageTimeLeft15m = (currentAverage / 1000).toFixed(0) + "mA (" + getTimeLeft(currentAverage, currentCharge) + ")";
              }
            )
        }

        pageTimeLeft = AccuBattery.isCharging() ? getTimeLeft(currentNumber, chargeFullNumber - currentCharge) : getTimeLeft(currentNumber, currentCharge);
        coverNow = (AccuBattery.isCharging() ? "" : "-") + (currentNumber / 1000).toFixed(0) + "mA ("+ pageTimeLeft +")";
    }

    function getTimeLeft(enteringCapacity, missingCapacity) {
        if (enteringCapacity > 0) {
            var timeLeft = (missingCapacity / enteringCapacity)
            var hoursLeft = Math.ceil(timeLeft);
            var minutesLeft = Math.ceil((timeLeft * 60) % 60);
            return hoursLeft + "h "+ minutesLeft + "m";
        } else {
            return "n.a.";
        }
    }

    initialPage: Component { FirstPage { } }
    cover: Qt.resolvedUrl("cover/CoverPage.qml")
    allowedOrientations: defaultAllowedOrientations
}
