/*
 Copyright (C) 2020 Dica-Developer.
 Contact: team@dica-developer.org
 All rights reserved.

 This file is part of harbour-sailfish-accubattery.

 harbour-sailfish-accubattery is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 harbour-sailfish-accubattery is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with harbour-sailfish-accubattery.  If not, see <http://www.gnu.org/licenses/>.
*/

function getPowerConsumption() {
    return (isCharging() ? "" : "-") + Math.ceil(getPowerConsumptionNumber() / 1000) + "mA";
}

function getPowerConsumptionNumber() {
    var result = 0;
    if (files.exists("/sys/class/power_supply/battery/current_now")) {
        result = Math.abs(parseInt(files.read("/sys/class/power_supply/battery/current_now").slice(0, -1), 10));
    }
    return result;
}

function getHealth() {
    var result = "";
    if (files.exists("/sys/class/power_supply/battery/health")) {
        result = files.read("/sys/class/power_supply/battery/health").slice(0, -1);
    }
    return result;
}

function getCapacity() {
    var result = "";
    if (files.exists("/sys/class/power_supply/battery/capacity")) {
        result = files.read("/sys/class/power_supply/battery/capacity").slice(0, -1) + "%";
    }
    return result;
}

function getCapacityNumber() {
    var result = "";
    if (files.exists("/sys/class/power_supply/battery/capacity")) {
        result = files.read("/sys/class/power_supply/battery/capacity").slice(0, -1);
    }
    return result;
}

function getChargeFull() {
    var result = "";
    if (files.exists("/sys/class/power_supply/battery/charge_full")) {
        result = files.read("/sys/class/power_supply/battery/charge_full").slice(0, -4) + "mAh";
    }
    return result;
}

function getChargeFullNumber() {
    var result = "";
    if (files.exists("/sys/class/power_supply/battery/charge_full")) {
        result = parseInt(files.read("/sys/class/power_supply/battery/charge_full").slice(0, -1), 10);
    }
    return result;
}

function getChargeFullDesign() {
    var result = "";
    if (files.exists("/sys/class/power_supply/battery/charge_full_design")) {
        result = files.read("/sys/class/power_supply/battery/charge_full_design").slice(0, -4) + "mAh";
    }
    return result;
}

function getChargeType() {
    var result = "";
    if (files.exists("/sys/class/power_supply/battery/charge_type")) {
        result = files.read("/sys/class/power_supply/battery/charge_type").slice(0, -1);
    }
    return result;
}

function isCharging() {
    var result = true;
    if ('N/A' === getChargeType()) {
        result = false;
    }
    return result;
}

function getVoltageNow() {
    var result = "";
    if (files.exists("/sys/class/power_supply/battery/voltage_now")) {
        result = files.read("/sys/class/power_supply/battery/voltage_now").slice(0, -4) + "mV";
    }
    return result;
}

function getInputCurrentNow() {
    var result = "";
    if (files.exists("/sys/class/power_supply/battery/input_current_now")) {
        result = files.read("/sys/class/power_supply/battery/input_current_now").slice(0, -1);
    }
    return result;
}

function getCurrentCharge() {
    var result = 0;
    if (files.exists("/sys/class/power_supply/battery/capacity")) {
        result = parseInt(files.read("/sys/class/power_supply/battery/capacity").slice(0, -1), 10) / 100;
    }
    if (files.exists("/sys/class/power_supply/battery/charge_full")) {
        result = result * parseInt(files.read("/sys/class/power_supply/battery/charge_full").slice(0, -1), 10);
    }
    return result;
}
