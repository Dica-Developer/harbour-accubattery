import QtQuick 2.0
import Sailfish.Silica 1.0

CoverBackground {

    Column {
        id: column

        anchors.top: parent.top
        anchors.left: parent.left
        anchors.topMargin: 6
        anchors.leftMargin: 8
        width: parent.width - (2 * anchors.leftMargin)

        Label {
            anchors.horizontalCenter: parent.horizontalCenter
            font.pixelSize: Theme.fontSizeMedium
            color: Theme.highlightColor
            text: app.isCharge ? qsTr("Charge") : qsTr("Discharge")
        }

        Rectangle {
            height: 3
            width: parent.width
            color: "transparent"
        }

        Row {
            width: parent.width

            Label
            {
                width: parent.width/2
                color: Theme.secondaryColor
                font.pixelSize: Theme.fontSizeTiny
                anchors.verticalCenter: parent.verticalCenter
                text: qsTr("Now")
            }

            Label
            {
                width: parent.width/2
                color: Theme.primaryColor
                font.pixelSize: Theme.fontSizeSmall
                horizontalAlignment: Text.AlignRight
                text: app.coverNow
            }
        }

        Row {
            width: parent.width

            Label
            {
                width: parent.width/2
                color: Theme.secondaryColor
                font.pixelSize: Theme.fontSizeTiny
                anchors.verticalCenter: parent.verticalCenter
                text: qsTr("Avg")
            }

            Label
            {
                width: parent.width/2
                color: Theme.primaryColor
                font.pixelSize: Theme.fontSizeSmall
                horizontalAlignment: Text.AlignRight
                text: app.coverAverage
            }
        }

        CoverActionList {
            id: coverAction

            CoverAction {
                iconSource: timer.interval<=1000 ? 'image://theme/icon-cover-pause' : 'image://theme/icon-cover-play'
                onTriggered: {
                    if (timer.interval <= 1000) {
                        timer.interval = 60000;
                    } else {
                        timer.interval = 1000;
                    }
                }
            }
        }
    }
}
