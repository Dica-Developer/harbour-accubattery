#ifdef QT_QML_DEBUG
#include <QtQuick>
#endif

#include <src/fileutils.h>

#include <sailfishapp.h>

int main(int argc, char *argv[])
{
    qmlRegisterType<FileUtils>("harbour.sailfish.browser.search.engine.manager.dicadevelopers.utils.Files", 1, 0, "Files");
    return SailfishApp::main(argc, argv);
}
